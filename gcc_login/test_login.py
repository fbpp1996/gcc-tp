import unittest
from django.test import Client
from django.contrib.auth.models import User

class SimpleTest(unittest.TestCase):
    #manage.py test test_login.SimpleTest
    #python manage.py test
    def setUp(self):
        # Every test needs a client.
        self.client = Client()
    def test_pagina(self):
        response = self.client.get("/accounts/login/")
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        user = User.objects.create(username='gcc')
        user.set_password('gcclogin')
        user.save()
        try:
            self.assertEqual(self.client.login(username='gcc', password='gcclogin'), True)
            user.delete()
        except AssertionError:
            user.delete()
            raise
