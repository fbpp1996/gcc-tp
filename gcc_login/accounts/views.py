from django.shortcuts import render
from django.contrib.auth import logout
from django.shortcuts import redirect

def index(request):
    return render(request, 'accounts/index.html')

def home(request):
    if not request.user.is_authenticated:
        return redirect('/accounts/login')
    else:
        return render(request, 'accounts/profile.html')

def user_logout(request):
    logout(request)
    return redirect('/accounts/login')

